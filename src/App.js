import React, { Component } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import history from './constants/history';
import Login from './pages/Login';
import LowonganJob from './pages/Lowongan Job';
import LamaranJob from './pages/Lamaran Job';
import { Provider } from 'react-redux';
import store from './constants/store';


import 'bootstrap/dist/css/bootstrap.min.css';
import 'antd/dist/antd.css';

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Provider store = { store }>
          <Router history={history}>
            <Routes>
              <Route path="/" Component={Login} element={<Login />} />
              <Route path="/lowongan-kerja" Component={LowonganJob} element={<LowonganJob />} />
              <Route path="/lamaran-kerja" Component={LamaranJob} element={<LamaranJob />} />
            </Routes>
          </Router>
        </Provider>
      </React.Fragment>
    );
  }
}

export default App;