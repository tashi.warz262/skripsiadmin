import { AUTH_ERROR, AUTH_LOGIN, GET_JOB, AUTH_LOGOUT, RESET_ERROR, AUTH_TAMBAH_JOB, AUTH_UPDATE_JOB, AUTH_DELETE_JOB, GET_PARTICIPANT, GET_FILE_CV, UPDATE_PESERTA, SET_USER } from "./type";

const initState = {
    admin: null,
    error: null,
    Job: null,
    Participant: null,
    message: null,
    progress: null,
    fetching: false,
    CVFile: null,
}

const rootReducer = (state = initState, action) => {
    const { type, payload, error, message } = action;
    switch(type) {
      case 'reset':
        return initState;
      case AUTH_LOGIN:
        return {
          ...state,
          admin: payload,
          message
        };
      case AUTH_TAMBAH_JOB:
        return {
          ...state,
          message,
          fetching: true
        }
      case AUTH_UPDATE_JOB:
        return {
          ...state,
          message,
          fetching: true
        }
      case AUTH_DELETE_JOB:
        return {
          ...state,
          message,
          fetching: true
        }
      case UPDATE_PESERTA:
        return {
          ...state,
          message,
          fetching: true
        }
      case AUTH_LOGOUT:
        return {
          ...state,
          admin: null,
          error: null,
          message: null,
          Job: null,
          progress: null,
          Participant: null,
        };
      case GET_JOB:
        return {
          ...state,
          Job: payload,
          fetching: false
        };
      case GET_PARTICIPANT:
        return  {
          ...state,
          Participant: payload,
          fetching: false,
        }
      case GET_FILE_CV: {
        return {
          ...state,
          CVFile: payload
        }
      }
      case AUTH_ERROR:
        return {
          ...state,
          error
        };
      case RESET_ERROR:
        return {
          ...state,
          error: null,
          message: null
        };
      case SET_USER:
        return {
          ...state,
          admin: payload
        }
      default:
        return state;
    }
}

export default rootReducer;