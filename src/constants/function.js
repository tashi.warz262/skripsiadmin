import { useNavigate } from 'react-router-dom'
import routes from "./routes";

export function LowonganJob() {
  const navigate = useNavigate();
  navigate(routes.LOWONGAN_JOB());
}

export function Login(){
  const navigate = useNavigate();
  navigate(routes.LOGIN_ADMIN());
}