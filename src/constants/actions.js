import axios from 'axios';
import { AUTH_ERROR, AUTH_LOGIN, AUTH_LOGOUT, GET_JOB, RESET_ERROR, AUTH_TAMBAH_JOB, AUTH_DELETE_JOB, AUTH_UPDATE_JOB, GET_PARTICIPANT, UPDATE_PESERTA, SET_USER } from './type';
import { endpoints } from './api';

export const tambahJob = (token, payload) => dispatch => {
    const config = { headers: { Authorization: `Bearer ${token}` } };
    axios.post(endpoints.tambahJob, payload, config)   
    .then(res => dispatch({ 
        type: AUTH_TAMBAH_JOB,
        message: res.data.message
    }))
    .catch(err => {
        dispatch({
            type: AUTH_ERROR,
            error: err.response.data.message
        })
    })
}

export const updateJob = (token, payload, id) => dispatch => {
    const config = { headers: { Authorization: `Bearer ${token}` } };
    axios.put(endpoints.updateJob(id), payload, config)   
    .then(res => dispatch({ 
        type: AUTH_UPDATE_JOB,
        message: res.data.message
    }))
    .catch(err => {
        dispatch({
            type: AUTH_ERROR,
            error: err.response.data.message
        })
    })
}

export const deleteJob = (token, id) => dispatch => {
    const config = { headers: { Authorization: `Bearer ${token}` } };
    axios.delete(endpoints.deleteJob(id), config)   
    .then(res => dispatch({ 
        type: AUTH_DELETE_JOB,
        message: res.data.message
    }))
    .catch(err => {
        dispatch({
            type: AUTH_ERROR,
            error: err.response.data.message
        })
    })
}

export const loginAdmin = (user) => dispatch => {
    axios.post(endpoints.loginAdmin, user)
            .then(res => {
                dispatch({
                    type: AUTH_LOGIN,
                    payload: res.data.admin,
                    message: res.data.message
                })
            })
            .catch(err => {
                dispatch({
                    type: AUTH_ERROR,
                    error: err.response.data.message
                })
            });
}

export const getJob = (token) => dispatch => {
    const config = { headers: { Authorization: `Bearer ${token}` } };
    axios.get(endpoints.getJob, config).then(
        res => {
            dispatch({
                type: GET_JOB,
                payload: res.data.data
            })
        }
    ).catch(err => {
        dispatch({
            type: AUTH_ERROR,
            error: err.response.data.message
        })
    })
}

export const getParticipant = (token) => dispatch => {
    const config = { headers: { Authorization: `Bearer ${token}` } };
    axios.get(endpoints.getParticipant, config).then(
        res => {
            dispatch({
                type: GET_PARTICIPANT,
                payload: res.data.data
            })
        }
    ).catch(err => {
        dispatch({
            type: AUTH_ERROR,
            error: err.response
        })
    })
}

export const updatePeserta = (token, id, payload) => dispatch => {
    const config = { headers: { Authorization: `Bearer ${token}` } };
    axios.put(endpoints.updatePeserta(id), payload, config).then(
      res => {
        dispatch({
          type: UPDATE_PESERTA,
          message: res.data.message
        })
      }
    ).catch(err => {
      dispatch({
        type: AUTH_ERROR,
        error: err.response.data.message
      })
    })
}

export const logoutAdmin = () => {
    return {
        type: AUTH_LOGOUT
    }
}

export const resetError = () => {
    return{
        type: RESET_ERROR
    }
}

export const setAdmin = (payload) => dispatch => {
    dispatch({
        type: SET_USER,
        payload
    })
}