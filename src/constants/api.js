export const baseUrl = ' https://peaceful-chamber-49198.herokuapp.com';

export const endpoints = {
  getJob: baseUrl + '/admin/v1/job-vacancy',
  getParticipant: baseUrl + '/admin/v1/job-applications',
  loginAdmin: baseUrl + '/admin/v1/login',
  tambahJob: baseUrl + `/admin/v1/job-vacancy`,
  updateJob: (id) => baseUrl + `/admin/v1/job-vacancy/${id}`,
  deleteJob: (id) => baseUrl + `/admin/v1/job-vacancy/${id}`,
  downloadCV: (id) => baseUrl + `/admin/v1/download-file-cv/${id}`,
  updatePeserta: (id) => baseUrl + `/admin/v1/update-progress-status/${id}`
}