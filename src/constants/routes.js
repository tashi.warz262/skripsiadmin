const routes = {
  LOGIN_ADMIN() {
    return `/`;
  },
  LOWONGAN_JOB() {
    return `/lowongan-kerja`;
  },
  LAMARAN_JOB() {
    return `/lamaran-kerja`;
  }
}

export default routes;