import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducer';
import { persistReducer } from 'redux-persist';
import storage from './storage';

const inititalState = {
  admin: null,
  error: null,
  Job: null,
  message: null,
  progress: null,
};

const enhancers = compose(
  typeof window !== 'undefined' && process.env.NODE_ENV !== 'production'
    ? window.devToolsExtension && window.devToolsExtension() : f => f
);

const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);

const makeConfiguredStore = (reducer, inititalState) => createStoreWithMiddleware(reducer, inititalState, enhancers);

const persistConfig = { key: 'nextJs', whitelist: ['auth'], storage };

const persistedReducer = persistReducer(persistConfig, rootReducer);
const store = makeConfiguredStore(persistedReducer, inititalState);

export default store;