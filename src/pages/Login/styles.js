const style = {
  Utama: {
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    width: '100%',
    backgroundColor: '#caf0f8',
    paddingTop: '50px',
    paddingBottom: '130px',
    '& .Judul': {
      fontSize: '64px'
    },
    '& .box': {
      marginTop: '100px',
      padding: '25px',
      borderRadius: '15px',
      backgroundColor: 'white',
      boxShadow: 'rgba(0,0,0,0.35) 0px 5px 15px',
      width: '650px',
      display: 'inline-block',
      '& .konten': {
        textAlign: 'left',
        '& .title': {
          fontSize: '36px',
          textAlign: 'left',
        },
        '& input': {
          fontSize: '18px',
          width: '100%',
          borderWidth: '1px'
        },
      },
      '& Button': {
        marginTop: '25px',
        borderStyle: 'solid',
        borderRadius: '10%',
        backgroundColor: '#19E121',
        color: 'white',
        fontWeight: '500',
        fontSize: '16px',
        width: '100px',
        height: '40px',
        cursor: 'pointer',
        '&:disabled':{
          backgroundColor: 'grey',
          color: 'white',
          cursor: 'not-allowed'
        }
      }
    }
  },
  footer: {
    backgroundColor: 'black',
    justifyContent: 'center',
    textAlign: 'center',
    '& .info':{
      color: 'white',
      fontSize: '19px',
    }
  },
  Modal: {
    width: '100%',
    height: '100%',
    backgroundColor: 'transparant',
    position: 'fixed',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 100,
    '& .isiKonten': {
      width: '500px',
      height: '250px',
      borderRadius: '12px',
      backgroundColor: 'white',
      boxShadow: 'rgba(0,0,0,0.35) 0px 5px 15px',
      display: 'flex',
      flexDirection: 'column',
      padding: '25px',
      justifyContent: 'center',
      alignItems: 'center',
      '& .Judul': {
        textAlign: 'center',
        '& h2.JudulError': {
          color: 'red'
        },
        '& h2.JudulSuccess': {
          color: '#19E121'
        },
      },
      '& .ButtonKonten': {
        marginTop: '40px',
        textAlign: 'center',
        '& Button.ButtonYa': {
          fontSize: '18px',
          cursor: 'pointer',
          borderStyle: 'solid',
          borderRadius: '10%',
          textAlign: 'capitalize',
          backgroundColor: '#19E121',
          color: 'white',
          height: '100%',
          width: '100px',
        },
        '& Button.ButtonTolak': {
          backgroundColor: 'red',
          marginLeft: '80px',
          fontSize: '18px',
          cursor: 'pointer',
          borderStyle: 'solid',
          borderRadius: '10%',
          textAlign: 'capitalize',
          color: 'white',
          height: '100%',
          width: '100px',
        }
      }
    }
  },
}

export default style;