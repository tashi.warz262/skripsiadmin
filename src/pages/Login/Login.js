import React, { createRef } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Formik } from 'formik';
import { Button } from 'antd';
import { Footer } from 'antd/lib/layout/layout';
import { isNil } from 'ramda';

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      ModalError: false,
      ModalSuccess: false,
      login: false
    }
    this.form = createRef();
  }

  componentDidUpdate(prevProps) {
    const { error, message } = this.props
    if(error !== prevProps.error && !isNil(error)) this._ModalError();
    if(message !== prevProps.message && !isNil(message)) this._ModalSuccess();
  }

  _closeModalError = () => {
    const { actions } = this.props;
    this.setState({ login: false });
    actions.resetError();
    this.setState({ ModalError: false });
  }

  _closeModalSuccess = () => {
    const { actions, admin: { token } } = this.props;
    this.setState({ login: false });
    window.localStorage.setItem('userId', this.props.admin.userId);
    window.localStorage.setItem('username', this.props.admin.username);
    window.localStorage.setItem('token', token);
    actions.resetError();
    actions.getJob(token);
    actions.getParticipant(token);
    this.setState({ ModalSuccess: false });
  }

  _ModalError = () => {
    this.setState({ ModalError: true });
  }

  _ModalSuccess = () => {
    this.setState({ ModalSuccess: true });
  }

  _formLogin = (values) => {
    const { values : { email, password } } = values;
    const { login } = this.state;
    return(
      <form>
        <div className='konten'>
          <span className='title'>Email:</span><br/>
          <input placeholder="Email" name="email" type="email" value={email} onChange={this._setValue.bind(this, 'email')}/>
          <br/><br/><span className='title'>Password:</span><br/>
          <input placeholder="Password" name="password" type="password" value={password} onChange={this._setValue.bind(this, 'password')}/>
        </div>
        <br/><Button onClick={this._LoginRequest} disabled={login || email === undefined || email === null || email === '' || password === undefined || password === null || password === '' }>Login</Button>
      </form>
    )
  }
  
  _setValue = (name, e) => {
    const { setFieldValue } = this.form.current;
    setFieldValue(name, e.target.value);
  }

  _LoginRequest = () => {
    const { values } = this.form.current;
    const { actions } = this.props;
    this.setState({ login: true });
    actions.loginAdmin(values);
  }

  render(){
    const { classes, error, message } = this.props;
    const { email, password, ModalError, ModalSuccess } = this.state;
    const initialValues = { email, password };
    return(
      <>
      {ModalError && <div className={classes.Modal}>
        <div className="isiKonten">
          <div className="Judul">
            <h2 className="JudulError">{error}</h2>
          </div>
          <div className="ButtonKonten">
            <Button onClick={this._closeModalError} className="ButtonYa">Ok</Button>
          </div>
        </div>
      </div>}
      {ModalSuccess && <div className={classes.Modal}>
        <div className="isiKonten">
          <div className="Judul">
            <h2 className="JudulSuccess">{message}</h2>
          </div>
          <div className="ButtonKonten">
            <Button onClick={this._closeModalSuccess} className="ButtonYa"><Link to="/lowongan-kerja">Ok</Link></Button>
          </div>
        </div>
      </div>}
      <div className={classes.Utama}>
        <h1 className="Judul">Selamat Datang<br/>Silahkan Login Terlebih Dahulu</h1>
        <div className="box">
          <Formik
            innerRef={this.form}
            initialValues={initialValues}
            onSubmit={this._LoginRequest}
            component={this._formLogin}
          />
        </div>
      </div>
      <Footer className={classes.footer}>
       <div className="info">
         PT. Frisian Flag Bandung<br/>Alamat: Jl. Batununggal Mulia I, Mengger, Kec. Bandung Kidul,<br/>Kota Bandung, Jawa Barat 40267<br/>Tlp: (022) 7506990
       </div>
     </Footer>
     </>
    )
  }
}

Login.propTypes = {
  admin: PropTypes.object.isRequired,
}