import Login from './Login';
import styles from './styles';
import withStyles from 'react-jss';
import { withRouter } from 'next/router';
import { connect } from 'react-redux';
import * as actions from '../../constants/actions';
import * as functions from '../../constants/function';
import { bindActionCreators } from 'redux';

const Styled = withStyles(styles)(Login);
const Routered = withRouter(Styled);
const mapStateToProps = ({ admin, error, Job, message, progress }) => ({ admin, error, Job, message, progress });
const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators({ ...actions, ...functions }, dispatch)
});
export default connect(mapStateToProps, mapDispatchToProps)(Routered);