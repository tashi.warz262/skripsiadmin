import React, { createRef } from 'react';
import PropTypes from 'prop-types';
import { Button, Table, Tooltip } from 'antd';
import { Link } from 'react-router-dom';
import { Header, Footer } from 'antd/lib/layout/layout';
import DatePicker from 'react-datepicker';
import moment from 'moment-timezone';
import { isNil } from 'ramda';
import { endpoints } from '../../constants/api';
import axios from 'axios';
import get from 'lodash.get'
import { Formik } from 'formik';
import 'react-datepicker/dist/react-datepicker.css';
import FileSaver from 'file-saver';

export default class LowonganJob extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ModalLogout: false,
      ModalError: false,
      ModalSuccess: false,
      ModalTolak: false,
      ModalTerima: false,
      fetching: false,
      tolak: false,
      terima: false,
      total: 0,
      page: 1,
      Tahap1: true,
      Tahap2: false,
      Tahap3: false,
      DataTable1: '',
      DataTable2: '',
      DataTable3: '',
      idPelamarUpdate: '',
      statusPelamarUpdate: '',
      date: '',
      time: '',
      location: '',
    }
    this.form = createRef();
  }

  componentDidMount() {
    if (this.props.admin === null || this.props.admin === undefined) this._setUser();
    this.props.actions.getParticipant(window.localStorage.getItem('token'));
  }

  componentDidUpdate(prevProps) {
    const { error, message } = this.props
    if(error !== prevProps.error && !isNil(error)) this._ModalError();
    if(message !== prevProps.message && !isNil(message)) this._ModalSuccess();
  }

  _setUser = () => {
    let name = window.localStorage.getItem('username');
    let id = window.localStorage.getItem('userId');
    let tkn = window.localStorage.getItem('token');
    let payload = { userId: id, username: name, token: tkn };
    this.props.actions.setAdmin(payload);
  }

  _participant = () => {
    const { Participant } = this.props;
    if(!isNil(Participant) && Participant !== undefined) {
      let data1 = Participant.filter(data => get(data, 'progressStatus_code.0') === 1);
      let data2 = Participant.filter(data => get(data, 'progressStatus_code.0') === 2);
      let data3 = Participant.filter(data => get(data, 'progressStatus_code.0') === 3);
      this.setState({ fetching: false, DataTable1: data1, DataTable2: data2, DataTable3: data3 });
    }
  }

  _renderTable = () => {
    this.props.actions.getParticipant(this.props.admin.token);
    this.setState({ fetching: true });
  }

  _ModalError = () => {
    if(this.state.terima) this.setState({ terima: false });
    if(this.state.tolak) this.setState({ tolak: false });
    this.setState({ ModalError: true });
  }

  _ModalSuccess = () => {
    if(this.state.terima) this.setState({ terima: false });
    if(this.state.tolak) this.setState({ tolak: false });
    if(this.state.ModalTolak || this.state.ModalTerima || this.state.ModalTerima2 || this.state.ModalTerima3) this.setState({ ModalTolak:false, ModalTerima: false, ModalTerima2: false, ModalTerima3: false });
    this._renderTable();
    this.setState({ ModalSuccess: true });
  }

  _closeModalError = () => {
    const { actions } = this.props;
    actions.resetError();
    this.setState({ ModalError: false });
  }

  _closeModalSuccess = () => {
    const { actions } = this.props;
    actions.resetError();
    this._participant();
    this.setState({ ModalSuccess: false });
  }

  _logoutRequest = () => {
    this.props.actions.logoutAdmin();
    this.setState({ ModalLogout: false });
    window.localStorage.clear();
  }

  _actionUpdate = (token, idPelamarUpdate, payload) => this.props.actions.updatePeserta(token, idPelamarUpdate, payload);

  _tolakPeserta = () => {
    const { idPelamarUpdate, statusPelamarUpdate } = this.state;
    const { admin: { token } } = this.props;
    this.setState({ tolak: true });
    let code = [4, statusPelamarUpdate];
    let payload = { progressStatus_code: code };
    let tkn = token === undefined ? window.localStorage.getItem('token') : token;
    this._actionUpdate(tkn, idPelamarUpdate, payload);
  }

  _terimaPeserta = () => {
    const { idPelamarUpdate, statusPelamarUpdate } = this.state;
    const { admin: { token } } = this.props;
    const { values } = this.form.current;
    let tkn = token === undefined ? window.localStorage.getItem('token') : token;
    this.setState({ terima: true });
    const waktu = values.time;
    const jam = waktu.split(':')[0];
    const menit = waktu.split(':')[1];
    const gabung = moment(values.date).add(moment.duration(jam, 'hours')).add(moment.duration(menit, 'minutes')).format();
    let code = [statusPelamarUpdate, statusPelamarUpdate-1]
    let payload = { datetime: gabung, location: values.location, progressStatus_code: code };
    this._actionUpdate(tkn, idPelamarUpdate, payload);
  }

  _handleOpenForm = () => this.setState({ ModalBuat: true });

  _downloadCV = (text, record) => {
    const genCVURL = endpoints.downloadCV(text);
    this._loadCV(genCVURL, (genLink) => FileSaver.saveAs(genLink, `CurriculumVitae_${record.user.username}.pdf`));
  }

  _updateTolak = (record) => {
    let code = get(record, 'progressStatus_code.0');
    this.setState({ idPelamarUpdate: record._id , statusPelamarUpdate: code, ModalTolak: true });
  }

  _updateTerima = (record) => {
    let code = get(record, 'progressStatus_code.0');
    this.setState({ idPelamarUpdate: record._id , statusPelamarUpdate: code });
    if(code === 1) this.setState({ ModalTerima: true });
    else if(code === 2) this.setState({ ModalTerima2: true });
    else this.setState({ ModalTerima3: true });
  }

  _loadCV = (link, url) => {
    const { admin: { token } } = this.props;
    axios({
      url: link,
      method: 'GET',
      responseType: 'blob',
      headers: {
        Authorization: `Bearer ${token}`
      }
    }).then(
      (response) => url(URL.createObjectURL(new Blob([response.data], { type: 'application/pdf' })))
    );
  }

  _renderCV = (text, record) => {
    const { classes } = this.props;
    return(
      <div className="CV">
        <span className={classes.btnCV} onClick={this._downloadCV.bind(this, text, record)}>{text}</span>
      </div>
    )
  }

  _renderJadwal = (record) => {
    const { classes } = this.props;
    return(
      <div className="jadwal">
        <p className={classes.jadwal}>{'Pada Hari : ' + moment(record.datetime).format('dddd') + ', ' + moment(record.datetime).format('Do MMMM YYYY')}</p>
        <p className={classes.jadwal}>{'Pada Jam : ' + moment(record.datetime).format('LT')}</p>
      </div>
    )
  }

  _renderAksi = (record) => {
    const { classes } = this.props;
    const today = new Date();
    return (
      <div className="aksiWrapper">
        <Tooltip title="Rejection" placement="bottom">
          <Button className={classes.marginRight1rem} onClick={this._updateTolak.bind(this, record)} disabled={(record.datetime !== undefined || record.datetime !== null) && moment(today).isBefore(record.datetime)}>
            <img src='tolak.png' rel="icon" alt="logo" className="logotolak" />
          </Button>
        </Tooltip>
        <Tooltip title="Approval" placement="bottom">
          <Button className={classes.marginRight1rem} onClick={this._updateTerima.bind(this, record)} disabled={(record.datetime !== undefined || record.datetime !== null) && moment(today).isBefore(record.datetime)}>
            <img src='terima.png' rel="icon" alt="logo" className="logo"/>
          </Button>
        </Tooltip>
      </div>
    );
  }

  _ModalUpdate = (values) => {
    const { values: { date, time, location } } = values
    const { terima, statusPelamarUpdate } = this.state;
    const day = new Date();
    day.setDate(day.getDate() + 2)
    return(
      <form>
        <div className="Konten">
          <div className='Kanan'>
            <div className='date-wrapper'>
              <div className='date-mini'>
                <span className='Judul'>{statusPelamarUpdate === 1 ? 'Tanggal Tes:' : 'Tanggal Wawancara :'}</span><br/>
                <DatePicker
                  title=''
                  format={'DD/MM/YYYY'}
                  className='input'
                  minDate={day}
                  onChange={this._setValue.bind(this, 'date')}
                  value={date}
                  showYearDropdown
                  scrollableMonthYearDropdown
                  isClearable
                />
              </div>
              <div>
                <span className='Judul'>{statusPelamarUpdate === 1 ? 'Jam Tes:' : 'Jam Wawancara :'}</span><br/>
                <input type='time' className='input' value={time} onChange={this._setValue.bind(this, 'time')}/>
              </div>
            </div>
            <div className='box'>
              <span className='Judul'>Lokasi:</span><br/>
              <textarea placeholder="location" type="text" name="location" value={location} onChange={this._setValue.bind(this, 'location')} />
            </div>
          </div>
        </div>
        <div className="ButtonKonten">
          <Button onClick={this._terimaPeserta} disabled={terima || date === undefined || date === null || date === '' || time === undefined || time === null || time === '' || location === undefined || location === null || location === '' }>Kirim</Button>
        </div>
      </form>
    )
  }

  _getParticipant = () => this.props.actions.getParticipant(this.props.admin.token);

  _activeTable1 = () => {
    this._participant();
    this.setState({ Tahap1: true, Tahap2: false, Tahap3: false });
  }

  _activeTable2 = () => {
    this._participant();
    this.setState({ Tahap1: false, Tahap2: true, Tahap3: false });
  }

  _activeTable3 = () => {
    this._participant();
    this.setState({ Tahap1: false, Tahap2: false, Tahap3: true });
  }

  _LogoutModal = () => {
    this.setState({ ModalLogout: true });
  }

  _Update = () => {
    this.setState({ ModalUpdate: false });
  }

  _closeModal = () => this.setState({ ModalLogout: false, ModalUpdate: false, ModalDeleteLowongan: false, jobId: null, ModalTerima: false, ModalTerima2: false, ModalTerima3: false, ModalTolak: false });

  _closeModalDetail = () => this.setState({ ModalDetail: false, formValue: null });

  _setValue = (name, e) => {
    const { setFieldValue } = this.form.current;
    if (name === 'date') {
      e = moment(e).format('YYYY-MM-DD');
      setFieldValue(name, e);
      this.setState({ date: e });
    }
    else if (name === 'time') {
      setFieldValue(name, e.target.value);
      this.setState({ time: e.target.value });

    }
    else {
      setFieldValue(name, e.target.value);
      this.setState({ location: e.target.value });
    }
  }

  render(){
    const { classes, error, message, admin } = this.props;
    const { ModalLogout, ModalSuccess, ModalError, ModalTolak, ModalTerima, ModalTerima2, ModalTerima3, Tahap1, Tahap2, Tahap3, DataTable1, DataTable2, DataTable3, date, time, location, tolak, terima, fetching } = this.state;
    const formValue = { date, time, location };
    const columns = [
        {
          title: 'Nama Pelamar',
          dataIndex: ['user','username'],
          key:'name',
          showSorterTooltip: false,
          width: 200,
        },
        {
          title: 'Pekerjaan yang dilamar',
          dataIndex: ['jobVacancy','jobTitle'],
          key: 'tglbuka',
          showSorterTooltip:false,
          width: 200,
        },
        {
          title: 'File CV',
          dataIndex: 'cvFile',
          key:'tgltutup',
          showSorterTooltip:false,
          width: 200,
          render: this._renderCV
        },
        {
          title: <p className="titleAksi">Aksi</p>,
          key: 'aksi',
          width: 40,
          render: this._renderAksi
        },
    ];
    const columns2 = [
      {
        title: 'Nama Pelamar',
        dataIndex: ['user','username'],
        key:'name',
        showSorterTooltip: false,
        width: 200,
      },
      {
        title: 'Pekerjaan yang dilamar',
        dataIndex: ['jobVacancy','jobTitle'],
        key: 'job',
        showSorterTooltip:false,
        width: 200,
      },
      {
        title: 'Jadwal',
        key:'jadwal',
        showSorterTooltip:false,
        width: 200,
        render: this._renderJadwal
      },
      {
        title: <p className="titleAksi">Aksi</p>,
        key: 'aksi',
        width: 40,
        render: this._renderAksi
      },
  ];
    return(
      <div>
        {ModalTerima && <div className={classes.KotakModal}>
          <div className="isiKonten">
            <div className={'closeModal'}>
              <Button onClick={this._closeModal} disabled={terima}>x</Button>
            </div>
            <div className="Judul">
              <h2>Approve CV Pelamar Kerja</h2>
              <h4>Silahkan masukan informasi berupa tanggal, waktu, dan lokasi untuk tes kemampuan pada form ini!</h4>
            </div>
            <Formik
              innerRef={this.form}
              component={this._ModalUpdate}
              initialValues={formValue}
              onSubmit={this._Update}
            />
          </div>
        </div>}
        {ModalTerima2 && <div className={classes.KotakModal}>
          <div className="isiKonten">
            <div className={'closeModal'}>
              <Button onClick={this._closeModal} disabled={terima}>x</Button>
            </div>
            <div className="Judul">
              <h2>Approve Hasil Tes Kemampuan</h2>
              <h4>Silahkan masukan informasi berupa tanggal, waktu, dan lokasi untuk tes wawancara pada form ini!</h4>
            </div>
            <Formik
              innerRef={this.form}
              component={this._ModalUpdate}
              initialValues={formValue}
              onSubmit={this._Update}
            />
          </div>
        </div>}
        {ModalTerima3 && <div className={classes.KotakModal}>
          <div className="isiKonten">
            <div className={'closeModal'}>
              <Button onClick={this._closeModal} disabled={terima}>x</Button>
            </div>
            <div className="Judul">
              <h2>Approve Hasil Tes Wawancara</h2>
              <h4>Silahkan masukan informasi berupa tanggal, waktu, dan lokasi untuk tes wanwancara pada form ini!</h4>
            </div>
            <Formik
              innerRef={this.form}
              component={this._ModalUpdate}
              initialValues={formValue}
              onSubmit={this._Update}
            />
          </div>
        </div>}
        {ModalError && <div className={classes.Modal}>
          <div className="isiKonten">
            <div className="Judul">
              <h2 className="JudulError">{error}</h2>
            </div>
            <div className="ButtonKonten">
              <Button onClick={this._closeModalError} className="ButtonYa">Ok</Button>
            </div>
          </div>
        </div>}
        {ModalSuccess && <div className={classes.Modal}>
          <div className="isiKonten">
            <div className="Judul">
              <h2 className="JudulSuccess">{message}</h2>
            </div>
            <div className="ButtonKonten">
              <Button onClick={this._closeModalSuccess} className="ButtonYa">Ok</Button>
            </div>
          </div>
        </div>}
        {ModalLogout && <div className={classes.ModalLogout}>
          <div className="isiKonten">
            <div className="Judul">
              <h2>Apakah Anda Yakin Ingin Logout?</h2>
            </div>
            <div className="ButtonKonten">
              <Button onClick={this._logoutRequest} className="ButtonYa"><Link to="/">Ya</Link></Button>
              <Button onClick={this._closeModal} className="ButtonTolak">Tidak</Button>
            </div>
          </div>
        </div>}
        {ModalTolak && <div className={classes.ModalLogout}>
          <div className="isiKonten">
            <div className="Judul">
              <h2>Apakah Anda Yakin Ingin Menolak Pelamar Tersebut?</h2>
            </div>
            <div className="ButtonKonten">
              <Button onClick={this._tolakPeserta} className="ButtonYa" disabled={tolak}>Ya</Button>
              <Button onClick={this._closeModal} className="ButtonTolak" disabled={tolak}>Tidak</Button>
            </div>
          </div>
        </div>}
        <Header className={classes.header}>
          <div className="leftcontent">
            <img src='FrisianFlag.png' rel="icon" alt="logo" className="logo" />
            <span className="SubMenu">
              <Link to="/lowongan-kerja"><span className="subtitle1">Lowongan Kerja</span></Link>
              <Link to="/lamaran-kerja"><span className="subtitle2" onClick={this._getParticipant}>Lamaran Kerja</span></Link>
            </span>
          </div>
          <div className="rightcontent">
            <span className="Name">Hi, {admin !== null && admin.username !== null ? admin.username : window.localStorage.getItem('username')}</span>
            <Button className="ButtonLogout" onClick={this._LogoutModal} disabled={ModalLogout}>Logout</Button>
          </div>
        </Header>
        <div className={classes.Utama}>
          <div className="konten">
            <h1 className="informasiJob">Informasi Lamaran Kerja</h1>
            <h4 className='subtitle'>Kelola data <span>lamaran kerja</span> pada tabel ini!</h4>
            <div className="btnTahapLamaran">
              <span className="subtitle1" onClick={this._activeTable1}>Tahap Menerima CV</span>
              <span className="subtitle1" onClick={this._activeTable2}>Tahap Tes Kemampuan</span>
              <span className="subtitle1" onClick={this._activeTable3}>Tahap Wawancara</span>
            </div>
            {Tahap1 && <Table
              className={classes.tabelku}
              columns={columns}
              dataSource={DataTable1}
              loading={fetching}
              pagination={{ position: ['none'] }}
            />}
            {Tahap2 && <Table
              className={classes.tabelku}
              columns={columns2}
              dataSource={DataTable2}
              loading={fetching}
              pagination={{ position: ['none'] }}
            />}
            {Tahap3 && <Table
              className={classes.tabelku}
              columns={columns2}
              dataSource={DataTable3}
              loading={fetching}
              pagination={{ position: ['none'] }}
            />}
          </div>
        </div>
        <Footer className={classes.footer}>
          <div className="info">
            PT. Frisian Flag Bandung<br/>Alamat: Jl. Batununggal Mulia I, Mengger, Kec. Bandung Kidul,<br/>Kota Bandung, Jawa Barat 40267<br/>Tlp: (022) 7506990
          </div>
        </Footer>
      </div>
    )
  }
}

LowonganJob.propTypes = {
  admin: PropTypes.object.isRequired,
};