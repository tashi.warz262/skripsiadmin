const style = {
    Utama:{
      justifyContent: 'center',
      width: '100%',
      minHeight: '808px',
      display: 'flex',
      '& div.konten': {
        width: '100%',
        backgroundColor: '#caf0f8',
        textAlign: 'center',
        '& h1.informasiJob': {
          marginTop: '120px',
          textAlign: 'center',
          textTransform: 'uppercase',
        },
        '& .subtitle':{
          textAlign: 'center',
          fontWeight: 'normal',
          marginBottom: '40px',
          '& span': {
            fontWeight: 700
          }
        },
        '& .btnTahapLamaran': {
          fontWeight: '700',
          fontSize: '16px',
          marginBottom: '50px',
          '& .subtitle1': {
            padding: '20px 15px 15px',
            borderBottom: '1px solid black',
            color: 'black',
            marginRight: '20px',
            cursor: 'pointer',
            '&:hover': {
              color: '#19E121',
              borderBottom: '1px solid #19E121'
            },
          },
        },
      },
    },
    Modal: {
      width: '100%',
      height: '100%',
      backgroundColor: 'transparant',
      position: 'fixed',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      zIndex: 100,
      '& .isiKonten': {
        width: '500px',
        height: '250px',
        borderRadius: '12px',
        backgroundColor: 'white',
        boxShadow: 'rgba(0,0,0,0.35) 0px 5px 15px',
        display: 'flex',
        flexDirection: 'column',
        padding: '25px',
        justifyContent: 'center',
        alignItems: 'center',
        '& .Judul': {
          textAlign: 'center',
          '& h2.JudulError': {
            color: 'red'
          },
          '& h2.JudulSuccess': {
            color: '#19E121'
          },
        },
        '& .ButtonKonten': {
          marginTop: '40px',
          textAlign: 'center',
          '& Button.ButtonYa': {
            fontSize: '18px',
            cursor: 'pointer',
            borderStyle: 'solid',
            borderRadius: '10%',
            textAlign: 'capitalize',
            backgroundColor: '#19E121',
            color: 'white',
            height: '100%',
            width: '100px',
          },
          '& Button.ButtonTolak': {
            backgroundColor: 'red',
            marginLeft: '80px',
            fontSize: '18px',
            cursor: 'pointer',
            borderStyle: 'solid',
            borderRadius: '10%',
            textAlign: 'capitalize',
            color: 'white',
            height: '100%',
            width: '100px',
          }
        }
      }
    },
    KotakModal: {
      width: '100%',
      height: '100%',
      backgroundColor: 'transparant',
      position: 'fixed',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      zIndex: 99,
      '& .isiKonten': {
        width: '600px',
        height: '550px',
        borderRadius: '12px',
        backgroundColor: 'white',
        boxShadow: 'rgba(0,0,0,0.35) 0px 5px 15px',
        display: 'flex',
        flexDirection: 'column',
        padding: '5px 25px 25px',
        justifyContent: 'center',
        alignItems: 'center',
        '& .closeModal': {
          display: 'flex',
          justifyContent: 'flex-end',
          float: 'right',
          paddingLeft: '560px',
          '& Button': {
            backgroundColor: 'transparent',
            border: 'none',
            fontSize: '25px',
            cursor: 'pointer'
          }
        },
        '& .Judul': {
          textAlign: 'center',
        },
        '& .Konten': {
          display: 'flex',
          width: '100%',
          marginTop: '10px',
          bosSizing: 'border-box',
          flex: 1,
          '& .Kanan': {
            '& .date-wrapper': {
              display: 'flex',
              boxSizing: 'border-box',
              '& div.date-mini': {
                flex: 1,
                bosSizing: 'border-box',
                width: '100%',
                paddingRight: '20px',
                marginBottom: '15px',
                '& .Judul': {
                  fontSize: '16px'
                },
                '& DatePicker': {
                  width: '100%',
                  fontSize: '16px'
                }
              }
            },
            '& .Judul': {
              fontSize: '16px'
            },
            '& DatePicker': {
              width: '100%',
              fontSize: '16px'
            },
            '& .box':{
              '& .Judul': {
                fontSize: '18px'
              },
              '& textarea': {
                width: '100%',
                height: '200px',
                fontSize: '16px',
                resize: 'none'
              }
            }
          },
        },
        '& .ButtonKonten': {
          marginTop: '10px',
          textAlign: 'center',
          '& Button': {
            fontSize: '18px',
            cursor: 'pointer',
            borderStyle: 'solid',
            borderRadius: '10%',
            textAlign: 'capitalize',
            backgroundColor: '#19E121',
            color: 'white',
            height: '100%',
            width: '100px',
            '&:disabled':{
              backgroundColor: 'grey',
              color: 'white',
              cursor: 'not-allowed'
            }
          }
        }
      },
      '& .Detail': {
        width: '800px',
        height: '500px',
        borderRadius: '12px',
        backgroundColor: 'white',
        boxShadow: 'rgba(0,0,0,0.35) 0px 5px 15px',
        display: 'flex',
        flexDirection: 'column',
        padding: '5px 25px 25px',
        justifyContent: 'center',
        alignItems: 'center',
        '& .closeModal': {
          display: 'flex',
          justifyContent: 'flex-end',
          float: 'right',
          paddingLeft: '700px',
          '& Button': {
            backgroundColor: 'transparent',
            border: 'none',
            fontSize: '25px',
            cursor: 'pointer'
          }
        },
        '& .Judul': {
          textAlign: 'center',
        },
        '& .Konten': {
          display: 'flex',
          width: '100%',
          marginTop: '30px',
          bosSizing: 'border-box',
          flex: 1,
          '& .Kiri': {
            bosSizing: 'border-box',
            '& .date-mini': {
              flex: 1,
              bosSizing: 'border-box',
              width: '100%',
              marginBottom: '10px',
              paddingRight: '20px',
              '& .Judul': {
                fontSize: '18px'
              },
              '& input': {
                width: '100%',
                fontSize: '16px',
                '&:disabled': {
                  cursor: 'not-allowed'
                }
              },
              '& select':{
                width: '100%',
                fontSize: '16px',
                '&:disabled': {
                  cursor: 'not-allowed'
                }
              }
            },
            '& .box':{
              paddingRight: '20px',
              '& .Judul': {
                fontSize: '18px'
              },
              '& textarea': {
                width: '350px',
                height: '200px',
                fontSize: '16px',
                resize: 'none',
                '&:disabled': {
                  cursor: 'not-allowed'
                }
              }
            }
          },
          '& .Kanan': {
            '& .date-wrapper': {
              display: 'flex',
              boxSizing: 'border-box',
              '& div.date-mini': {
                flex: 1,
                bosSizing: 'border-box',
                width: '100%',
                paddingRight: '20px',
                marginBottom: '15px',
                '& .Judul': {
                  fontSize: '16px'
                },
                '& DatePicker': {
                  width: '100%',
                  fontSize: '16px',
                  '&:disabled': {
                    cursor: 'not-allowed'
                  }
                }
              }
            },
            '& .Judul': {
              fontSize: '16px'
            },
            '& DatePicker': {
              width: '100%',
              fontSize: '16px'
            },
            '& .box':{
              '& .Judul': {
                fontSize: '18px'
              },
              '& textarea': {
                width: '100%',
                height: '200px',
                fontSize: '16px',
                resize: 'none',
                '&:disabled': {
                  cursor: 'not-allowed'
                }
              }
            }
          },
        },
        '& .ButtonKonten': {
          marginTop: '40px',
          textAlign: 'center',
          '& Button': {
            fontSize: '18px',
            cursor: 'pointer',
            borderStyle: 'solid',
            borderRadius: '10%',
            textAlign: 'capitalize',
            backgroundColor: '#19E121',
            color: 'white',
            height: '100%',
            width: '100px',
          }
        }
      }
    },
    header: {
      backgroundColor: 'white',
      position: 'fixed',
      display: 'flex',
      width: '100%',
      height: '90px',
      zIndex: '10',
      justifyContent: 'center',
      '& .leftcontent': {
        marginRight: '700px',
        '& .logo':{
          height: '6em',
          marginRight: '30px'
        },
        '& .SubMenu': {
          paddingTop: '15px',
          cursor: 'pointer',
          fontWeight: '700',
          fontSize: '18px',
          height: '100%',
          position: 'absolute',
          '& .subtitle1': {
            padding: '20px 15px 30px',
            borderBottom: '1px solid black',
            color: 'black',
            '&:hover': {
              color: '#19E121',
              borderBottom: '1px solid #19E121'
            },
          },
          '& .subtitle2': {
            padding: '20px 15px 30px',
            borderBottom: '1px solid black',
            color: 'black',
            '&:hover': {
              color: '#19E121',
              borderBottom: '1px solid #19E121'
            },
          }
        },
      },
      '& .rightcontent': {
        '& .Name': {
          marginTop: '25px',
          marginRight: '30px',
          fontSize: '20px',
          textTransform: 'capitalize',
          color: 'black',
        },
        '& .ButtonLogout': {
          marginTop: '25px',
          borderStyle: 'solid',
          borderRadius: '10%',
          backgroundColor: 'red',
          color: 'white',
          textTransform: 'capitalize',
          fontWeight: '500',
          fontSize: '16px',
          width: '100px',
          height: '40px',
          cursor: 'pointer',
          marginRight: '50px',
          '&:disabled':{
            backgroundColor: 'grey',
            color: 'white',
            cursor: 'not-allowed'
          }
        },
      }
    },
    tabelku: {
      marginLeft: '200px',
      marginRight: '200px',
      marginBottom: '100px',
      overflow: 'auto',
      '& .ant-table-expanded-row-fixed': {
        backgroundColor: '#fafafa'
      },
      '& .ant-pagination-next:hover .ant-pagination-item-link, .ant-pagination-prev:hover .ant-pagination-item-link': {
        color: "teal60",
        borderColor: "teal60"
      },
      '& .ant-pagination-disabled:hover .ant-pagination-item-link': {
        color: 'rgba(0, 0, 0, 0.25)',
        borderColor: '#d9d9d9',
        cursor: 'not-allowed',
      },
      '& .ant-pagination-item-active, .ant-pagination-item:focus, .ant-pagination-item:hover': {
        borderColor: "teal60",
        '& a': {
          color: "teal60"
        }
      },
      '& .ant-pagination-item, .ant-pagination-item-link': {
        borderRadius: '4px',
      },
      '& .table-icon': {
        fontSize: 16,
        cursor: 'pointer',
        '&.edit': {
          color: '#229BD8'
        },
        '&.eye': {
          color: '#74B816'
        },
        '&.trash': {
          color: '#DE1B1B'
        },
        '&.document' : {
          color: '#F76707'
        }
      },
      '& .ant-table-thead > tr > th': {
        backgroundColor: '#EEEEEE',
        color: '#212121',
        fontWeight: '700',
        fontFamily: 'Lato, sans-serif'
      },
      '& .ant-table-column-sorters': {
        paddingRight: 0,
        '& > span:nth-of-type(1)': {
          marginLeft: '17px',
        },
        '& > span:nth-of-type(2)': {
          position: 'absolute',
          marginLeft: 0,
        },
      },
      '& .ant-dropdown-trigger': {
        alignItems: 'center',
        justifyContent: 'center',
        display: 'flex',
      },
      '& .ant-pagination-total-text': {
        position: 'absolute',
        left: 18,
        color: "grey60",
        fontFamily: 'Lato, sans-serif',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: '13px',
        lineHeight: '15px',
        display: 'flex',
        alignItems: 'center',
      },
      '& .ant-pagination-disabled': {
        opacity: .3
      },
      '& .ant-table-ping-right': {
        '& .ant-table-cell-fix-right-first': {
          '&:after': {
            boxShadow: 'unset'
          }
        }
      },
      '& .ant-table-ping-left': {
        '&:not(.ant-table-has-fix-left)': {
          '& .ant-table-container': {
            '&:before': {
              boxShadow: 'unset'
            }
          }
        }
      }
    },
    btnCV:{
      cursor: 'pointer',
      textDecoration: 'underline',
      color: 'blue',
    },
    marginRight1rem: {
      marginRight: '1rem',
      cursor: 'pointer',
      '&:disabled': {
        cursor: 'not-allowed'
      },
      '& .logo': {
        height: '1.5em',
      },
      '& .logotolak': {
        height: '16px',
        width: '21px'
      }
    },
    footer: {
      backgroundColor: 'black',
      justifyContent: 'center',
      textAlign: 'center',
      '& .info':{
        color: 'white',
        fontSize: '18px',
      }
    },
    ModalLogout: {
      width: '100%',
      height: '100%',
      backgroundColor: 'transparant',
      position: 'fixed',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      zIndex: 100,
      '& .isiKonten': {
        width: '500px',
        height: '250px',
        borderRadius: '12px',
        backgroundColor: 'white',
        boxShadow: 'rgba(0,0,0,0.35) 0px 5px 15px',
        display: 'flex',
        flexDirection: 'column',
        padding: '25px',
        justifyContent: 'center',
        alignItems: 'center',
        '& .Judul': {
          textAlign: 'center',
          '& h2.JudulError': {
            color: 'red'
          },
          '& h2.JudulSuccess': {
            color: '#19E121'
          },
        },
        '& .ButtonKonten': {
          marginTop: '40px',
          textAlign: 'center',
          '& Button.ButtonYa': {
            fontSize: '18px',
            cursor: 'pointer',
            borderStyle: 'solid',
            borderRadius: '10%',
            textAlign: 'capitalize',
            backgroundColor: '#19E121',
            color: 'white',
            height: '100%',
            width: '100px',
          },
          '& Button.ButtonTolak': {
            backgroundColor: 'red',
            marginLeft: '80px',
            fontSize: '18px',
            cursor: 'pointer',
            borderStyle: 'solid',
            borderRadius: '10%',
            textAlign: 'capitalize',
            color: 'white',
            height: '100%',
            width: '100px',
          }
        }
      }
    },
  }
  
  export default style;