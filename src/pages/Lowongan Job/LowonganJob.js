import React, { createRef } from 'react';
import PropTypes from 'prop-types';
import { Button, Table, Tooltip } from 'antd';
import { Link } from 'react-router-dom';
import { Header, Footer } from 'antd/lib/layout/layout';
import DatePicker from 'react-datepicker';
import { Formik } from 'formik';
import moment from 'moment-timezone';
import { isNil } from 'ramda';
import 'react-datepicker/dist/react-datepicker.css';

export default class LowonganJob extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ModalUpdate: false,
      ModalDeleteLowongan: false,
      ModalBuat: false,
      ModalDetail: false,
      ModalLogout: false,
      ModalError: false,
      ModalSuccess: false,
      tambah: false,
      update: false,
      del: false,
      total: 0,
      page: 1,
      jobId: '',
      jobTitle: '',
      jobDescription: '',
      jobRequirements: '',
      startDate: '',
      endDate: '',
      jobStatus: '',
      formValue: null,
    }
    this.form = createRef();
  }
  componentDidMount () {
    if (this.props.admin === null || this.props.admin === undefined) this._setUser();
    this.props.actions.getJob(window.localStorage.getItem('token'));
  }

  componentDidUpdate(prevProps) {
    const { error, message, fetching } = this.props
    if(error !== prevProps.error && !isNil(error)) this._ModalError();
    if(message !== prevProps.message && !isNil(message)) this._ModalSuccess();
    if(fetching) this._renderTable();
  }

  _setUser = () => {
    let name = window.localStorage.getItem('username');
    let id = window.localStorage.getItem('userId');
    let tkn = window.localStorage.getItem('token');
    let payload = { userId: id, username: name, token: tkn };
    this.props.actions.setAdmin(payload);
  }

  _renderTable = () => this.props.actions.getJob(this.props.admin.token);

  _getParticipant = () => this.props.actions.getParticipant(this.props.admin.token);

  _ModalError = () => {
    const { tambah, update, del } = this.state;
    if(tambah) this.setState({ tambah: false });
    if(update) this.setState({ update: false });
    if(del) this.setState({ del: false });
    this.setState({ ModalError: true });
  }

  _ModalSuccess = () => {
    const { tambah, update, del } = this.state;
    if(tambah) this.setState({ tambah: false });
    if(update) this.setState({ update: false });
    if(del) this.setState({ del: false });
    if(this.state.ModalBuat) this.setState({ ModalBuat:false });
    if(this.state.ModalUpdate) this.setState({ ModalUpdate:false });
    this.setState({ ModalSuccess: true });
  }

  _closeModalError = () => {
    const { actions } = this.props;
    actions.resetError();
    this.setState({ ModalError: false });
  }

  _closeModalSuccess = () => {
    const { actions } = this.props;
    actions.resetError();
    actions.getJob(this.props.admin.token);
    this.setState({ ModalSuccess: false });
  }

  _logoutRequest = () => {
    this.props.actions.logoutAdmin();
    this.setState({ ModalLogout: false });
    window.localStorage.clear();
  }

  _handleDetail = (record) => {
    this.setState({
      ModalDetail: true,
      formValue:{
        jobTitle: record.jobTitle,
        jobDescription: record.jobDescription,
        jobStatus: record.jobStatus,
        startDate: moment(record.startDate).format('YYYY-MM-DD'),
        endDate: moment(record.endDate).format('YYYY-MM-DD'),
        jobRequirements: record.jobRequirements
      }
    });
  }

  _handleUpdate = (record) => {
    this.setState({
      ModalUpdate: true,
      formValue:{
        jobTitle: record.jobTitle,
        jobDescription: record.jobDescription,
        startDate: moment(record.startDate).format('YYYY-MM-DD'),
        endDate: moment(record.endDate).format('YYYY-MM-DD'),
        jobRequirements: record.jobRequirements,
        jobStatus: record.jobStatus
      },
      jobId: record._id,
    })
  }

  _handleDelete = (record) => {
    this.setState({
      ModalDeleteLowongan: true,
      jobId: record._id
    });
  }

  _handleOpenForm = () => this.setState({ ModalBuat: true });

  _renderTgl = text => <span>{moment(text).format('DD/MM/YYYY')}</span>

  _renderAksi = (record) => {
    const { classes } = this.props;
    return (
      <div className="aksiWrapper">
        <Tooltip title="Detail" placement="bottom">
          <span className={classes.marginRight1rem} onClick={this._handleDetail.bind(this, record)}>
            <img src='Eye.png' rel="icon" alt="logo" className="logo" />
          </span>
        </Tooltip>
        <Tooltip title="Ubah" placement="bottom">
          <span className={classes.marginRight1rem} onClick={this._handleUpdate.bind(this, record)}>
            <img src='Edit.png' rel="icon" alt="logo" className="logo" />
          </span>
        </Tooltip>
        <Tooltip title="Hapus" placement="bottom">
          <span className={classes.marginRight1rem} onClick={this._handleDelete.bind(this, record)}>
            <img src='Trash.png' rel="icon" alt="logo" className="logo" />
          </span>
        </Tooltip>
      </div>
    );
  }

  _TambahLowongan = () => {
    const { values } = this.form.current;
    const { actions, admin: { token } } = this.props;
    this.setState({ tambah: true });
    let tkn = token === undefined ? window.localStorage.getItem('token') : token;
    actions.tambahJob(tkn, values);
  }

  _UpdateLowongan = () => {
    const { jobId } = this.state;
    const { values } = this.form.current;
    const { actions, admin: { token } } = this.props;
    let tkn = token === undefined ? window.localStorage.getItem('token') : token;
    this.setState({ update: true });
    actions.updateJob(tkn, values, jobId);
  }

  _deleteLowongan = () => {
    const { jobId } = this.state;
    const{ actions, admin: { token } } = this.props;
    let tkn = token === undefined ? window.localStorage.getItem('token') : token;
    this.setState({ update: true });
    actions.deleteJob(tkn, jobId);
    this.setState({ ModalDeleteLowongan: false, jobId: null });
  }

  _ModalBuat = (values) => {
    const { values: { jobTitle, jobDescription, jobRequirements, startDate, endDate } } = values;
    const { tambah } = this.state;
    return(
      <form>
        <div className="Konten">
          <div className='Kiri'>
            <div className='date-mini'>
              <span className='Judul'>Pekerjaan:</span><br/>
              <input placeholder="Pekerjaan" name="jobTitle" type="text" value={jobTitle} onChange={this._setValue.bind(this, 'jobTitle')}/>
            </div>
            <div className='box'>
              <span className='Judul'>Deksripsi Pekerjaan:</span><br/>
              <textarea placeholder="Deskripsi" type="text" name="jobDescription" value={jobDescription} onChange={this._setValue.bind(this, 'jobDescription')} />
            </div>
          </div>
          <div className='Kanan'>
          <div className='date-wrapper'>
              <div className='date-mini'>
                <span className='Judul'>Tanggal Dibuka:</span><br/>
                <DatePicker
                  title=''
                  format={'DD/MM/YYYY'}
                  className='input'
                  minDate={new Date()}
                  onChange={this._setValue.bind(this, 'startDate')}
                  value={startDate}
                  showYearDropdown
                  scrollableMonthYearDropdown
                  isClearable
                />
              </div>
              <div>
                <span className='Judul'>Tanggal Ditutup:</span><br/>
                <DatePicker
                  format={'DD/MM/YYYY'}
                  minDate={new Date()}
                  onChange={this._setValue.bind(this, 'endDate')}
                  className='input'
                  value={endDate}
                  showYearDropdown
                  scrollableMonthYearDropdown
                  isClearable
                />
              </div>
            </div>
            <div className='box'>
              <span className='Judul'>Spesifikasi Pekerjaan:</span><br/>
              <textarea placeholder="Spesifikasi" type="text" name="jobRequirements" value={jobRequirements} onChange={this._setValue.bind(this, 'jobRequirements')} />
            </div>
          </div>
        </div>
        <div className="ButtonKonten">
          <Button onClick={this._TambahLowongan} disabled={tambah || jobTitle === undefined || jobTitle === null || jobTitle === '' ||
           jobDescription === undefined || jobDescription === null || jobDescription === '' || jobRequirements === undefined || jobRequirements === null || jobRequirements === '' ||
           startDate === undefined || startDate === null || startDate === '' || endDate === undefined || endDate === null || endDate === ''}>Tambah</Button>
        </div>
      </form>
    )
  }

  _ModalUpdate = (values) => {
    const { values: { jobTitle, jobDescription, jobRequirements, startDate, endDate, jobStatus } } = values;
    const { update } = this.state;
    return(
      <form>
        <div className="Konten">
          <div className='Kiri'>
            <div className='date-mini'>
              <span className='Judul'>Pekerjaan:</span><br/>
              <input placeholder="Pekerjaan" name="jobTitle" type="text" value={jobTitle} onChange={this._setValue.bind(this, 'jobTitle')}/>
            </div>
            <div className='date-mini'>
              <span className='Judul'>Status:</span><br/>
              <select placeholder="Pekerjaan" name="jobTitle" type="text" value={jobStatus} onChange={this._setValue.bind(this, 'jobStatus')}>
                <option value="Open">Open</option>
                <option value="Closed">Closed</option>
              </select>
            </div>
            <div className='box'>
              <span className='Judul'>Deksripsi Pekerjaan:</span><br/>
              <textarea placeholder="Deskripsi" type="text" name="jobDescription" value={jobDescription} onChange={this._setValue.bind(this, 'jobDescription')} />
            </div>
          </div>
          <div className='Kanan'>
          <div className='date-wrapper'>
              <div className='date-mini'>
                <span className='Judul'>Tanggal Dibuka:</span><br/>
                <DatePicker
                  title=''
                  format={'DD/MM/YYYY'}
                  className='input'
                  minDate={new Date()}
                  onChange={this._setValue.bind(this, 'startDate')}
                  value={startDate}
                  showYearDropdown
                  scrollableMonthYearDropdown
                  isClearable
                />
              </div>
              <div>
                <span className='Judul'>Tanggal Ditutup:</span><br/>
                <DatePicker
                  format={'DD/MM/YYYY'}
                  minDate={new Date()}
                  onChange={this._setValue.bind(this, 'endDate')}
                  className='input'
                  value={endDate}
                  showYearDropdown
                  scrollableMonthYearDropdown
                  isClearable
                />
              </div>
            </div>
            <div className='box'>
              <span className='Judul'>Spesifikasi Pekerjaan:</span><br/>
              <textarea placeholder="Spesifikasi" type="text" name="jobRequirements" value={jobRequirements} onChange={this._setValue.bind(this, 'jobRequirements')} />
            </div>
          </div>
        </div>
        <div className="ButtonKonten">
          <Button onClick={this._UpdateLowongan} disabled={update}>Update</Button>
        </div>
      </form>
    )
  }

  _detail = () => {
    const { formValue: { jobTitle, jobDescription, startDate, endDate, jobRequirements, jobStatus } } = this.state;
    return(
      <form>
        <div className="Konten">
          <div className='Kiri'>
            <div className='date-mini'>
              <span className='Judul'>Pekerjaan:</span><br/>
              <input placeholder="Pekerjaan" name="jobTitle" type="text" value={jobTitle} disabled={true}/>
            </div>
            <div className='date-mini'>
              <span className='Judul'>Status:</span><br/>
              <select placeholder="Pekerjaan" name="jobTitle" type="text" value={jobStatus} disabled={true}>
                <option value="Open">Open</option>
                <option value="Closed">Closed</option>
              </select>
            </div>
            <div className='box'>
              <span className='Judul'>Deksripsi Pekerjaan:</span><br/>
              <textarea placeholder="Deskripsi" type="text" name="jobDescription" value={jobDescription} disabled={true}/>
            </div>
          </div>
          <div className='Kanan'>
          <div className='date-wrapper'>
              <div className='date-mini'>
                <span className='Judul'>Tanggal Dibuka:</span><br/>
                <DatePicker
                  title=''
                  format={'DD/MM/YYYY'}
                  className='input'
                  value={startDate}
                  showYearDropdown
                  scrollableMonthYearDropdown
                  isClearable
                  disabled={true}
                />
              </div>
              <div>
                <span className='Judul'>Tanggal Ditutup:</span><br/>
                <DatePicker
                  format={'DD/MM/YYYY'}
                  className='input'
                  value={endDate}
                  showYearDropdown
                  scrollableMonthYearDropdown
                  isClearable
                  disabled={true}
                />
              </div>
            </div>
            <div className='box'>
              <span className='Judul'>Spesifikasi Pekerjaan:</span><br/>
              <textarea placeholder="Spesifikasi" type="text" name="jobRequirements" value={jobRequirements} disabled={true}/>
            </div>
          </div>
        </div>
      </form>
    )
  }

  _LogoutModal = () => {
    this.setState({ ModalLogout: true });
  }

  _BuatBaru = () => {
    this.setState({ ModalBuat: false });
  }

  _Update = () => {
    this.setState({ ModalUpdate: false });
  }

  _closeModal = () => this.setState({ ModalBuat: false, ModalLogout: false, ModalUpdate: false, ModalDeleteLowongan: false, jobId: null, formValue: null });

  _closeModalDetail = () => this.setState({ ModalDetail: false, formValue: null });

  _setValue = (name, e) => {
    const { setFieldValue } = this.form.current;
    if (name === 'startDate' || name === 'endDate') {
      e = moment(e).format('YYYY-MM-DD');
      setFieldValue(name, e);
    }
    else setFieldValue(name, e.target.value);
  }

  render(){
    const { classes, Job, error, message, admin } = this.props;
    const { ModalLogout, ModalBuat, ModalSuccess, ModalError, ModalDetail, ModalDeleteLowongan, ModalUpdate, jobTitle, jobDescription, jobRequirements, startDate, endDate, formValue, tambah, update, del } = this.state;
    const initialValues = { jobTitle, jobDescription, jobRequirements, startDate, endDate };
    const columns = [
        {
          title: 'Nama Pekerjaan',
          dataIndex: 'jobTitle',
          key:'name',
          showSorterTooltip: false,
          width: 300,
        },
        {
          title: 'Tanggal Pendaftaran',
          dataIndex: 'startDate',
          key: 'tglbuka',
          showSorterTooltip:false,
          width: 200,
          render: this._renderTgl,
        },
        {
          title: 'Tanggal Tutup Pendaftaran',
          dataIndex: 'endDate',
          key:'tgltutup',
          showSorterTooltip:false,
          width: 200,
          render: this._renderTgl,
        },
        {
          title: <p className="titleAksi">Aksi</p>,
          key: 'aksi',
          width: 40,
          render: this._renderAksi
        },
      ];
    return(
      <div>
        {ModalError && <div className={classes.Modal}>
          <div className="isiKonten">
            <div className="Judul">
              <h2 className="JudulError">{error}</h2>
            </div>
            <div className="ButtonKonten">
              <Button onClick={this._closeModalError} className="ButtonYa">Ok</Button>
            </div>
          </div>
        </div>}
        {ModalSuccess && <div className={classes.Modal}>
          <div className="isiKonten">
            <div className="Judul">
              <h2 className="JudulSuccess">{message}</h2>
            </div>
            <div className="ButtonKonten">
              <Button onClick={this._closeModalSuccess} className="ButtonYa">Ok</Button>
            </div>
          </div>
        </div>}
        {ModalBuat && <div className={classes.KotakModal}>
          <div className="isiKonten">
            <div className={'closeModal'}>
              <Button onClick={this._closeModal} disabled={tambah}>x</Button>
            </div>
            <div className="Judul">
              <h2>Tambah Lowongan Kerja</h2>
              <h4>Silahkan tambah data informasi lowongan kerja pada form ini</h4>
            </div>
            <Formik
              innerRef={this.form}
              component={this._ModalBuat}
              initialValues={initialValues}
              onSubmit={this._BuatBaru}
            />
          </div>
        </div>}
        {ModalUpdate && <div className={classes.KotakModal}>
          <div className="isiKonten">
            <div className={'closeModal'}>
              <Button onClick={this._closeModal} disabled={update}>x</Button>
            </div>
            <div className="Judul">
              <h2>Update Lowongan Kerja</h2>
              <h4>Silahkan update data informasi lowongan kerja pada form ini</h4>
            </div>
            <Formik
              innerRef={this.form}
              component={this._ModalUpdate}
              initialValues={formValue}
              onSubmit={this._Update}
            />
          </div>
        </div>}
        {ModalDetail && <div className={classes.KotakModal}>
          <div className="Detail">
            <div className={'closeModal'}>
              <Button onClick={this._closeModalDetail}>x</Button>
            </div>
            <div className="Judul">
              <h2>Detail Lowongan Kerja</h2>
              <h4>Berikut data informasi lowongan kerja</h4>
            </div>
            {this._detail()}
          </div>
        </div>}
        {ModalDeleteLowongan && <div className={classes.ModalLogout}>
          <div className="isiKonten">
            <div className="Judul">
              <h2>Apakah Anda Yakin Ingin Menghapus Lowongan Ini?</h2>
            </div>
            <div className="ButtonKonten">
              <Button onClick={this._deleteLowongan} disabled={del} className="ButtonYa">Ya</Button>
              <Button onClick={this._closeModal} className="ButtonTolak" disabled={del}>Tidak</Button>
            </div>
          </div>
        </div>}
        {ModalLogout && <div className={classes.ModalLogout}>
          <div className="isiKonten">
            <div className="Judul">
              <h2>Apakah Anda Yakin Ingin Logout?</h2>
            </div>
            <div className="ButtonKonten">
              <Button onClick={this._logoutRequest} className="ButtonYa"><Link to="/">Ya</Link></Button>
              <Button onClick={this._closeModal} className="ButtonTolak">Tidak</Button>
            </div>
          </div>
        </div>}
        <Header className={classes.header}>
          <div className="leftcontent">
            <img src='FrisianFlag.png' rel="icon" alt="logo" className="logo" />
            <span className="SubMenu">
              <Link to="/lowongan-kerja"><span className="subtitle1">Lowongan Kerja</span></Link>
              <Link to="/lamaran-kerja"><span className="subtitle2" onClick={this._getParticipant}>Lamaran Kerja</span></Link>
            </span>
          </div>
          <div className="rightcontent">
            <span className="Name">Hi, {admin !== null && admin.username !== null ? admin.username : window.localStorage.getItem('username')}</span>
            <Button className="ButtonLogout" onClick={this._LogoutModal} disabled={ModalLogout}>Logout</Button>
          </div>
        </Header>
        <div className={classes.Utama}>
          <div className="konten">
            <h1 className="informasiJob">Informasi Lowongan Kerja</h1>
            <h4 className='subtitle'>Kelola data <span>lowongan kerja</span> pada tabel ini!</h4>
            <Button onClick={this._handleOpenForm} className="btnAdd">Tambah</Button>
            <Table
              className={classes.tabelku}
              columns={columns}
              dataSource={Job}
              pagination={{ position: ['none'] }}
            />
          </div>
        </div>
        <Footer className={classes.footer}>
          <div className="info">
            PT. Frisian Flag Bandung<br/>Alamat: Jl. Batununggal Mulia I, Mengger, Kec. Bandung Kidul,<br/>Kota Bandung, Jawa Barat 40267<br/>Tlp: (022) 7506990
          </div>
        </Footer>
      </div>
    )
  }
}

LowonganJob.propTypes = {
  admin: PropTypes.object.isRequired,
};